from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import Attendee, ConferenceVO
import json


class ConferenceVOEncoder:
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    import_href = f"api/conferences/{conference_vo_id}"
    conference_vo = ConferenceVO.objects.get(import_href=import_href)

    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    else:  # POST
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            #        conference = ConferenceVO.objects.get(id=conference_id)
            content["conference"] = conference_vo
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(
            request.body
        )  # DO SOMETHING ABOUT CONFERENCE HERE!!!
        # try:
        #     # new code
        #     if "state" in content:
        #         state = State.objects.get(abbreviation=content["state"])
        #         content["state"] = state
        # except State.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid state abbreviation"},
        #         status=400,
        #     )

        # new code
        Attendee.objects.filter(id=id).update(**content)

        # copied from get detail
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conferences": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # )


"""
showattende

else:
    content = json.loads(request.body)
    try:
        if "conference" in content:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conferen
    except Conference.DoesNotExist

"""
